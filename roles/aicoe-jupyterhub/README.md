AICoE-JupyterHub
================

AICoE-JupyterHub is a customized deployment of the [JupyterHub for OpenShift](https://github.com/jupyter-on-openshift/jupyterhub-quickstart) project. It uses the base Jupyter On OpenShift server image and adds a custom [JupyterHub OpenShift Authenticator](https://github.com/aicoe/jupyterhub-ocp-oauth) and [JupyterHub Singleuser Profiles](https://github.com/vpavlin/jupyterhub-singleuser-profiles) to the server image.

Requirements
------------

* [Radanalytics Spark Operator](https://github.com/radanalyticsio/spark-operator) provided by the Open Data Hub spark-operator component
* Any custom notebook images you need to use with JupyterHub should be built using [Jupyter Notebooks for OpenShift](https://github.com/jupyter-on-openshift/jupyter-notebooks)

Role Variables
--------------

* `deploy_all_notebooks` - Deploy builds for all of the extra notebooks including scipy & tensorflow
* `registry` - Name of the image registry where notebook images are stored. Prevents the creation of notebook buildconfigs. Requires a value for `repository`
* `repository` - Name of the repository in the `registry` where images are located. Requires a value for `registry`.
* `cookie_secret` - Sets the [JupyterHub Cookie Secret](https://jupyterhub.readthedocs.io/en/stable/getting-started/security-basics.html#cookie-secret) 
* `storage_class` - The OpenShift storageclass to use for PersistentVolumes
* `db_memory` - Set the memory resource value for the JupyterHub database pod
* `db_password` - Sets the password that is used during the creation of the PostgreSQL JupyterHub database
* `jupyterhub_memory` - Set the memory resource value for the JupyterHub pod
* `jupyterhub_db_version` - postgresql image version to pull from the openshift repository
* `notebook_image` - The notebook image to spawn for each new user
* `notebook_memory` - The memory resource value to use for the notebook pod
* `s3_endpoint_url` - The S3 endpoint that can be used to read/write data during Jupyter notebook execution. This value will be accessible as the environment variable `S3_ENDPOINT_URL` in all Jupyter notebook instances
* `jupyterhub_admins` - A list of user IDs that should have administrative rights
* `user_pvc_size` - A volume size set for users PVC
* `jupyterhub_configmap_name` - Name of custom pre-existing jupyterhub_config configmap to attach to the jupyterhub pod
* `spark_configmap_template` - Name of the `SparkCluster` configmap template that will be used as the config map for all dedicated user spark clusters when the user Jupyter instance is created. Default value: `jupyterhub-spark-operator-configmap`
* `spark_pyspark_submit_args` - The [PySpark arguments](https://spark.apache.org/docs/latest/configuration.html#available-properties) that will be provided to the user Jupyter instance in the environment variable `PYSPARK_SUBMIT_ARGS`
* `spark_pyspark_driver_python` - The name of the PySpark python driver that will be provided to the user Jupyter instance in the environment variable `PYSPARK_DRIVER_PYTHON`
* `spark_pyspark_driver_python_opts` - The PySpark driver options that will be provided to the user Jupyter instance in the environment variable `PYSPARK_DRIVER_PYTHON_OPTS`
* `spark_home` - The location of the PySpark library files on the user Jupyter instance. This value will be provided in the environment variable `SPARK_HOME` Default value: `/opt/app-root/lib/python3.6/site-packages/pyspark/`
* `spark_pythonpath` - Set this value to overried the defautl `PYTHONPATH` set in the user Jupyter instance
* `spark_worker_nodes` - The number of spark worker nodes to spawn when the user Jupyter instance is created
* `spark_master_nodes` - The number of spark maser nodes to spawn when the user Jupyter instance is created
* `spark_memory` - The amount of memory to allocate to each Spark node
* `spark_cpu` - The number of CPUs to allocate to each Spark node
* `spark_image` - The container image to use for each node in the Spark cluster

ALL role variables are optional and this role will deploy successfully using the default values

Dependencies
------------

None

Sample Configuration
--------------------

```
aicoe-jupyterhub:
  odh_deploy: true
  notebook_memory: 2Gi
  deploy_all_notebooks: False
  registry: ''
  repository: ''
  db_memory: 1Gi
  jupyterhub_memory: 1Gi
  notebook_image: 's2i-minimal-notebook:3.6'
  notebook_memory: 1Gi
  s3_endpoint_url: ''
  jupyterhub_admins:
    - admin
    - jsmith
    - jdoe
  user_pvc_size: 2Gi
  jupyterhub_configmap_name: example-custom-jupyterhub-cfg

  spark_configmap_template: 'jupyterhub-spark-operator-configmap'
  spark_pyspark_submit_args: "--conf spark.cores.max=6 --conf spark.executor.instances=2 --conf spark.executor.memory=3G --conf spark.executor.cores=3 --conf spark.driver.memory=4G --packages com.amazonaws:aws-java-sdk:1.7.4,org.apache.hadoop:hadoop-aws:2.7.3 pyspark-shell"
  spark_pyspark_driver_python: "jupyter"
  spark_pyspark_driver_python_opts: "notebook"
  spark_home: "/opt/app-root/lib/python3.6/site-packages/pyspark/"
  spark_pythonpath: "$PYTHONPATH:/opt/app-root/lib/python3.6/site-packages/:/opt/app-root/lib/python3.6/site-packages/pyspark/python/:/opt/app-root/lib/python3.6/site-packages/pyspark/python/lib/py4j-0.8.2.1-src.zip"

  spark_worker_nodes: 2
  spark_master_nodes: 1
  spark_memory: 4Gi
  spark_cpu: 3
  spark_image: "quay.io/opendatahub/spark-cluster-image:spark22python36"
```

Additional Information
----------------------

Deployment of individual Jupyter notebook pods is configured via [JupyterHub Singleuser Profiles](https://github.com/vpavlin/jupyterhub-singleuser-profiles).
The Open Data Hub Operator deploys a default set of profiles sufficient for common use cases. To allow for customization of these
profiles, JupyterHub will dynamically load profile definitions from Kubernetes ConfigMaps.

To leverage this feature, create a ConfigMap containing a file named "jupyterhub-singleuser-profiles.yaml". The contents of
this file should adhere to the profile definition standards documented in the [JupyterHub Singleuser Profiles](https://github.com/vpavlin/jupyterhub-singleuser-profiles)
project. To tell JupyterHub to read this ConfigMap, apply the label `jupyterhub=singleuser-profiles` to the ConfigMap object. When using this feature, it
is important to be mindful of the order in which profile definitions will be loaded. The default profiles defined by the Open Data Hub Operator
will be loaded first, then ConfigMaps with the matching label will be loaded alphabetically by the ConfigMap name. As profiles are loaded, options override
any settings defined in profiles that were loaded earlier.

The following is an example of a ConfigMap definition which will customize the JupyterHub profiles beyond the default set:

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-profiles-customization
  labels:
    jupyterhub: singleuser-profiles
data:
  jupyterhub-singleuser-profiles.yaml: |
    profiles:
      - name: My profile
        # A list of Jupyter notebook images (as listed in JupyterHub spawner UI) to which the profile is applied.
        images:
          - 's2i-minimal-notebook:3.6'
          - 's2i-tensorflow-notebook:3.6'
          - 'some-other-notebook-image:latest'
        env:
          MY_ENV_VAR: foobar
    sizes:
      - name: My Custom Size
        resources:
          mem_limit: 1Gi
          cpu_limit: 1
```

Modifying JupyterHub server behavior
------------------------------------
If you need to add/override any features of the jupyterhub server or modify the pod spec of notebook pods when they are spawned, you can supply an additional jupyterhub_config.py in a configmap that contains valid python code for modifying the [jupyterhub](https://jupyterhub-kubespawner.readthedocs.io/en/latest/).

In the ODH CR, you can specify the name of a configmap that contains a valid jupterhub_config.py to use as the additional config that will be mounted at `/opt/app-root/configs/jupterhub_config.py` in the jupyterhub server pod.  Any code you include in this configmap will be executed after to the default jupyterhub_config.py in `/opt/app-root/src/.jupyter/jupyterhub_config.py`

```yaml
# Example jupyterhub config to make OpenShift users "user99" and "ocp-cluster-admin-user" jupyterhub admins
apiVersion: v1
kind: ConfigMap
metadata:
  name: example-custom-jupyterhub-cfg
data:
  jupyterhub_config.py: "c.Authenticator.admin_users = ['user99', 'ocp-cluster-admin-user']"
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io
