Thrift Server
=============

Deploys the Spark SQL Thrift server to expose Spark dataframes modeled as Hive tables through a JDBC connection.

**NOTE:** This is a tech preview role.

Requirements
------------  

- Spark Cluster
- Hive metastore

Role Variables
--------------

* `aws_access_key_id` - Sets the AWS S3 Access Key ID
* `aws_secret_access_key` - Sets de AWS S3 Secret Access Key
* `s3_endpoint` - Sets the AWS S3

* `use_existing_spark_cluster` - Sets if the user want to use an existing Spark Cluster

* `master_node_count` - Sets the number of Spark Master nodes
* `master_memory` - Sets the amount of memory for Spark Master nodes
* `master_cpu` - Sets the amount of CPU for Spark Master nodes
* `worker_node_count` - Sets the number of Spark Worker nodes
* `worker_memory` - Sets the amount of memory for Spark Workers nodes
* `worker_cpu` - Sets the amount of CPU for Spark Worker nodes

* `spark_cluster_port` - Sets the port of the Spark cluster where Thrift server should connect to 
* `spark_max_cores` - Sets the maximum cores the thrift server executors can use


Dependencies
------------

- Hive Metastore

Sample Configuration
--------------------

```
thrift-server:
  spark_cluster_port: 7077
  spark_max_cores: 6
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io